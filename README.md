# README #

This README explains how to get started with the Intel Edison BSP. 

### Summary ###

* At the end of this you will get all the necessary files to flash your Intel Edison board
* Version: 1.0

### How do I get set up? ###

* Clone this repository
* run the go.sh file located in the conf directory
* All the source files should be extracted to the src directory
* Go to the src directory and run ./device-software/setup.sh
* You're all goo to start building the Edison image
* Follow the instructions displayed on your terminal
* Run "source poky/oe-init-build-env"
* Run "bitbake edison-image"

