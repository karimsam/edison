#!/bin/bash

set -e

OS=$(uname)
SED_CMD=sed

if [ "${OS}" == "Darwin" ]; then
  SED_CMD=gsed
fi

DOWNLOAD_DIR=../download/
SRC_DIR=../src/

source edison-src.txt

mkdir -p "${DOWNLOAD_DIR}"
mkdir -p "${SRC_DIR}"
# get the source tree
if [ ! -f "${DOWNLOAD_DIR}${EDISON_SRC}" ]; then
  echo "Getting the Intel Edison sources"
  wget -P "${DOWNLOAD_DIR}" "${EDISON_URL}${EDISON_SRC}"
fi

# remove the old installation 
rm -rf "${SRC_DIR}"*
# untar the archive
echo "Extracting the archive to src directory"
tar xzf "${DOWNLOAD_DIR}${EDISON_SRC}" -C "${SRC_DIR}"

#apply the patches
echo "Applying the patches if applicable"
# Fix issue when patching in a git repository
"${SED_CMD}" -e "s/git apply/patch -p1 </g" -i "${SRC_DIR}edison-src/device-software/setup.sh"
# Fix the issue when compiling on a 32-bts host
"${SED_CMD}" -e "s|export OPENSSL_CONF=\${TMPDIR}/sysroots/x86_64-linux/usr/lib/ssl/openssl.cnf|export OPENSSL_CONF=\${STAGING_LIBDIR}/ssl/openssl.cnf|g" \
             -i "${SRC_DIR}edison-src/device-software/meta-edison-distro/recipes-connectivity/libwebsockets/libwebsockets_1.23.bb"

# success
echo "Setup complete." 
